「⋯⋯咦，好奇怪啊⋯⋯？」

我們正在馬車裡休息，突然聽到馭手座上傳來這樣的自言自語
是亞蘇爾的聲音
我有些在意，於是掀開幌子探出頭

「⋯⋯怎麼了嗎？」

我這麼一問，亞蘇爾露出了為難的表情，說到

「啊，雷特老爺，感覺道路有點奇怪⋯⋯走的路應該沒錯，但總覺得路邊的景色和平時不一樣啊。按理說，差不多應該看得到村落了才對」
「這不是你的錯覺嗎？被搗蛋哥布林阻擋了之後，距離感偏差了之類的」
「⋯⋯唔──嗯，是嗎？說不清楚啊，不過，這之後，老爺們也偶爾露個臉，觀察下周圍吧？察覺到有什麼不對勁的話，請馬上告訴我」

他這樣回答
我點了點頭，回到車篷裡，對羅蕾露和奧格利講了一下

「好像迷路了，讓我們也小心點」

奧格利歪著頭

「⋯⋯啊？接下來要去的村落⋯⋯去盧莎村的路有那麼複雜嗎？雖然確實有好幾條岔路，但也不至於走錯吧」

之前確實看到街道上有幾個分岔口，可能是那時候走錯了，才會來到現在這莫名其妙的地方吧
這樣解釋的話就不奇怪了
但那些也不是什麼複雜的岔路，馭手又是有過多次經驗的老手
如果總是走錯路，早就丟掉飯碗了

「⋯⋯雖然被搗蛋哥布林群襲擊，但只要沿著接到走，應該沒那麼多容易走岔的地方吧。是因為驚慌失措走錯了麼？但是，魔物暫且不論，俗話說『不存在沒被盜賊襲擊過的旅行商人』。因為這種程度的事情就方向感錯亂的話，是做不來商人的吧」

羅蕾露歪著頭，說出了商人們之間的諺語
旅行商人雖然大多不及冒険者，但都會具有基本程度的自衛能力
商人自己拿起武器，與護衛的冒険者們共同戰鬥，並不是什麼稀奇的事情
熟悉這種氛圍的商人，要比一般市民膽子大的多
僅僅碰到一些危險，實在是不至於驚慌失措到走岔路
話雖如此⋯⋯現在已經這樣了

「若說有什麼疑點的話⋯⋯」

羅蕾露把手放在臉頰上，這樣說到

「什麼？」

我問到，羅蕾露回答

「可能被什麼幻惑了吧，那樣的話，感官就會發生混亂，會走錯路也就不奇怪了」
「幻惑？是說魔術嗎？」

奧格利問，羅琳搖了搖頭。

「不，感覺不到魔術的氣息，若真的如此，我應該馬上就會注意到的。應該是其他手段⋯⋯也許是藥？」
「藥麼⋯⋯這樣說的話，是何時被下藥了呢？」

奧格利的疑問應該說是理所當然的吧，但羅蕾露搖了搖頭

「不知道。之前我們也離開過馬車，也有可能是在王都時被人灌了什麼東西。現在很難弄清楚啊」

說不定是使用了什麼遲效性的藥物
這種可能性也不能說沒有
不能說沒有啊⋯⋯

「假設是那樣，為何要給亞蘇爾下藥？」

我有些疑惑
雖然這麼說有些失禮，亞蘇爾就算比一般的市民富裕一些，但應該也沒有特意策劃襲擊的價值
羅蕾露思考了一會，回答

「說不清楚啊，也許是有什麼冤家⋯⋯嗯，也說不定是衝著我們來的。但怎麼說都有些微妙啊？說起來，從我們雇了他的馬車開始，幾乎就都在一起，若是沖我們來的，就只能是那之後下的手。但對方應該沒有時間給他灌什麼東西才對⋯⋯硬要說的話，就只有剛才戰鬥的時候⋯⋯」
「若你猜得沒錯，亞蘇爾應該在那時喝了什麼東西」
「嘛，沒錯，但這些說到底也不過是可能性罷了。僅僅是亞蘇爾方向感不好的可能性也是存在的⋯⋯要想確認的話，就需要調查一下亞蘇爾本人了」
「說的沒錯啊，要馬上停車麼？」

我這樣問到，奧格利把頭探出窗外看了看，然後說

「⋯⋯天已經快黑了，恐怕今晚是到不了村莊，那麼亞蘇爾應該會提出露營的請求，那時候再檢查就好了吧？」

雖然只是可能性而已，但若這真的是有人謀劃的，他一定會在附近觀察著馬車，現在突然停下的話，可能會引起對方警戒
如果是在需要紮營的時候停下，也會顯得比較自然
態突然惡化的話，就馬上停車吧，但現在這樣，還是順其自然好一些

沒過多久，就和奧格利預測的一樣

「⋯⋯老爺們。對不住啊，果然好像是走錯路了。今天再怎麼加把勁也到不了村落了，所以，就在這附近露營如何呢⋯⋯？」

我點了點頭，答道

「不，我不介意。不過，你知道我們現在的位置嗎？」
「⋯⋯關於這個，也非常奇怪啊。對不住⋯⋯但是，往回走走的話，應該就能搞明白了。啊，對了，車費。都因為我的錯，才變成這個樣子，錢會退給你們，當然，也會好好把你們帶到目的地的⋯⋯」

他低著頭說出了道歉的話
看他現在的樣子，真的就只像個路痴
總而言之，今天看來是要在這裡露宿了

亞蘇爾似乎打算給我們提供保存食品，但我們自己也帶有食物
魔法袋裡也裝著烹飪工具，即使是在野外露宿，也能提供一定程度的熱食。不如一起吃吧，這樣一說，大家便都露出有些開心的表情