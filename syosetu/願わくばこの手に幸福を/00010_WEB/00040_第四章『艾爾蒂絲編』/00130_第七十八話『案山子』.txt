“——很遺憾，我拒絕。在這樣的溫室裡長大的公主，僅僅是愉快的幻想，是無法讓我託付生命的。我說過吧，我一點都不想死。”
這樣說出來後我就閉上嘴。

啊，說了。說了啊。我不知道為什麼要說。但是，必須說。不說的話，是不會成功的。
只是屈服於她的言語，被那個瞳孔所震懾，就什麼都不會變化。我的精神始終會被囚禁著。而且，正如我所說的那樣，我不打算參與這種簡陋的計劃。

嘴角扭曲，眼角發抖，膝部若沒有壓制住，就會陷入抽搐之中。屋子裡出現了一剎那的空白。沒有聲音，非常的寂靜。但那一定是暴風雨前的寧靜。

強行調整了表情，抬起頭。看到了艾爾蒂絲的碧眼。那雙眼睛。對，就是那雙眼睛，流露出令我害怕的光輝。
本就大的瞳孔被最大程度地睜開，碧眼中一片憤慨。那，或許不是覺得我說的有錯，只是在為我的言語而焦躁。

到那時我確信了。如果是過去，我已死於此地。如此的，深沉的焦躁。天啊，我，穿越了多麼危險的地段。
現在，壓制住艾爾蒂絲的是高貴者所具有的矜持和經過漫長的歲月培養出來的理性。那是我的生命線，是維繫生命的細線。

好可怕。害怕的不得了。
“什麼呀。那麼，我不想放棄這樣的話語是謊言嗎？”
艾爾蒂絲的話語貫穿了空間。聲音的出處，仿彿被情緒所動搖一樣，在顫抖，咬緊了臼齒。
“真討厭啊。所以我才討厭發出不負責言論的傢伙。結果你也只是為了撐場面而說些大話——”
憤怒的感情已經過去，現在流露出的是赤裸裸的嘲笑。真的是，像嘲笑螞蟻般的聲音。眼睛裡帶著嘲弄，深處卻還蘊含著一絲安心感。

媽的！怎麼回事？舌頭像麻了似地動不了。汗水順著脖子流淌下來。
現在我已經理解了。不在這裡讓這傢伙垂頭喪氣的話，那就完了。那樣的話，就不會有任何的改變。

但是，怎麼回事？我的身體，舌頭。被那雙眼睛瞪了一眼之後，身體和腦子都不肯動彈了。這是怎麼回事，這傢伙。

連風也像嘲笑一樣地在耳邊響起。
“到頭來，你也一樣啊，膽小鬼。什麼都做不了。聽說你多少還算是個知名的人。真不知道走到這個地步是有多好運。跟隨你的伙伴也真是沒有眼光，你的敵人肯定很弱吧。”
然後，精靈公主繼續著言語。
原來如此。說得太棒了。精彩。

這是什麼感覺？這是怎麼回事？心中仍隱藏著恐懼，腦海中還殘留著曾經像虫子一樣被踐踏的記憶。那是千真萬確的事實。
但是，從內心深處，涌出了什麼不同的東西。

這和曾經燒焦身體的憤怒是不同的。與覆蓋了全身的憎恨也是不同的。這到底是怎樣的情感呢？
但是，只有一件事我完全明白。

我果然，不管走到哪裡，都不過是臭老鼠罷了。然後我張開了嘴。
“不用說了，公主殿下。我是不會做出改變的，就是那樣。無論身處多麼綺麗的地方，我作為臭老鼠的事實都不會改變。這是我為數不多可以確定的事情。”
不知不覺，我從床上站了起來。原來這裡視野這麼開闊啊，因恐懼而收縮的心臟，此刻卻在高鳴著。
睜開眼睛，對著略顯動搖的碧眼，像是要灌輸什麼似的，拋出了話語。
“但是。有一件事我可以發誓。我的伙伴，還有敵人。他們毫無疑問，對，毫無疑問——是英雄。我確信如此，毋庸置疑。”
當然是那樣。我，對這個身體寄予了自信的事之類，幾乎沒有。怎麼可能有自信啊？趴在地上，以尊嚴為代價得以苟活的這具身體。怎麼可能有自信啊？

所以，如果要說有的話，那也只能是他們的事情了。劍技高手，魔術天才，還有毋庸置疑的英雄。
他們是我內心的驕傲，也是我應該憎恨的敵人。真是複雜的感情。用言語無法表達出的感情。那樣的他們，被當成笨蛋，被當成像我這樣的人一樣，不可原諒。感情涌現了出來。

啊，因恐怖而扭曲的嘴角，恢復了正常。
“所以，別拿我當借口。”
對著像是在驚愕似地張開嘴的艾爾蒂絲，這樣說道。那位已經隨心所欲地說過了。那麼這次該輪到我了吧。

心情大好。我此刻正把曾經的恐怖踩在腳下。
而且，有人不能不對這個女人說的事。
“是借口吧。因為，無論何時我詢問你，你都在辯解吧。”
艾爾蒂絲的嘴唇，抽動著。視線不知朝向何處，其中似乎失去了力量。
“想放棄的人是誰啊，艾爾蒂絲。是你自己吧？你不是想憑自己的意志放棄嗎？”
這個女人也有狡猾的地方啊，我這樣想著。剛才在嘲笑我時看到的安心的神色。果然沒看錯。太好了，如此一來，自己也不必再讓自己身處險境了。這樣想著就放心了。

我很了解那種心情。
說到底，艾爾蒂絲還沒能確定本心。正因為我說沒有放棄，所以才要說些挑釁的話而已。如果到了明天，也許會對瓦利安娜說“開個玩笑”而撤銷了今天的話語。

沒錯，現在的表情就是最好的證明。臉頰紅潤，蘊含著羞恥。眼神動搖著卻無言以對。
救世之旅時，她已經失去了理智，所以我無從得知。

艾爾蒂絲，精靈的公主啊，你和其他的英雄們不一樣。和卡麗婭・帕多利克，芙拉朵・娜・波魯克庫拉特，還有赫爾特·斯坦利不同。你不像他們那樣能力和精神都很優秀。只是，白白擁有力量的，可怜的女人。

畢竟，雖然擁有力量，卻自己把自己關在這座塔裡，一步也不敢外出的女人。
她的本性，一定非常膽小。和我一樣。
“公主。正如我所說，我不會聽你的話。所以，你來聽我的話吧。我會把你卷進我的漩渦裡，讓你無處可逃。”
為了證明這一點，我一邊這麼說著，一邊注視著艾爾蒂絲。對於這樣的我，艾爾蒂絲一句話也說不出口。只是驚恐地搖晃著碧眼。

啊，太好了。這個女人也是如此。
不是能掌握自己感情的英雄，只是被感情操縱的玩偶。

這樣的話。艾爾蒂絲，你也不再是我的敵人了。