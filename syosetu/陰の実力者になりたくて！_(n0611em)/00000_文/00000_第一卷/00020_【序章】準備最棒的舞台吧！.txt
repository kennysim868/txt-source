契機是什麼已經想不起來了。只是自懂事起我便已經對『影之實力者』心懷憧憬了

是因為動畫、漫畫、還是電影，不，無論是哪個都無所謂。對我而言只要是『影之實力者』，不管是怎麼樣的都好。

既非主人公、亦非最終BOSS，而是從暗中介入故事並展現實力的存在。我憧憬著那樣的影之實力者，同時也產生了想要成為那般存在的想法。就像無論是誰在孩童時都會憧憬英雄一樣，對我而言那便是影之實力者。僅此而已。

只是和那些憧憬英雄的孩子們不同，我的念想卻絕非是三分鐘熱度，而是在內心的深處不間斷地燃燒，無時不刻地推動著我。

空手道、拳擊、劍道、綜合格闘技……對於變強所必需的東西都全力去習得，然後無聲無息地隱藏起實力。只為了那或將到來的某一天。

在學校的時候貫徹平凡二字。扮演著決不顯眼的、人畜無害的龍套A。不過在日常的裏側，則是把時間全都耗費在了修行上。這便是我的青春、我的學生生活。

但是、隨著時間的推移不安也越發強烈。直面現實的時候終於還是來了。

是的、即使這麼做也是沒有意義的。

無論再怎麼學習隨處可見的格闘技，也沒有辦法得到像故事世界裡影之實力者那樣的、壓倒性的力量。我能做到的最多也就是教訓教訓幾個小混混的程度。面對持有遠程武器的對手就有點困難了，如果被全副武裝的軍人包圍的話那也就到此為止了。

被軍人暴揍的影之實力者……笑死人了。

哪怕未來幾十年我都持續修行，成為了世界最強的格闘家，被軍人包圍的話肯定還是會被暴揍的。不、或許還是有可能的。經過鍛鍊的人類即使被軍人的包圍說不定也能返揍回去。

可就算能打倒軍人，如果腦袋上掉下核彈的話還是會被蒸發，這就是人類的極限。只有這點我可以斷言。我所憧憬的影之實力者是不會被核彈所蒸發的。所以我也必須成為不會被核彈蒸發的人類。

為了不被核彈蒸發所必要的東西是什麼。

是拳力嗎。

是鋼鐵般的肉體嗎。

是無限的體力嗎。

不是那樣的東西。

而是某種、更為異樣的力量。

沒錯，魔力、瑪納、氣、靈氣，無論是什麼都好。得到未知的力量是必須的。

這就是我直面現實後所得到的答案。

假設這裡有一個試圖尋找魔力的人。

無論是誰肯定都會懷疑他的腦袋是否正常吧。換做我也一樣。肯定會懷疑他腦子正不正常的吧。

可是、事實又真是如此嗎。

這個世界上還沒有人能證明魔力的存在。不過與此同時也沒有人能證明魔力不存在。

用正常的思維是無法得到我想要的力量的。而那一定就在瘋狂彼端。

自此之後的修行變得極為困難。

魔力、瑪納、氣、靈氣，沒人知道習得這些的方法。

我嘗試了坐禪、在瀑布下打坐、冥想、絕食、瑜伽、改教、尋找精靈、向神祈禱、把自己釘在十字架上。不存在所謂的正確答案。在黑暗之中我只是前行在自己所堅信的道路上。

然後轉瞬即逝，我迎來了高中最後的夏天。然而無論是魔力、瑪納、氣、還是靈氣，都還沒能找到……

◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

結束了往常的修行後周圍已經完全變暗了。

我穿上之前脫下丟在一邊的內衣褲後，披上了校服。雖然還是沒能掌握未知的力量。但從最近一直持續的修行中，還是能感受到一些什麼的。

現在也是如此。

結束修行後的身體，總覺得腦袋裡閃耀著光芒，視野也在搖晃著。

魔力……又或者是靈氣嗎……能夠確切地感受到它的影響。

今天的修行也很充實呢。

通過在森林裡脫光衣服以全*裸的姿態去感受森羅萬象，通過不斷用頭撞擊大樹來物理上的排除雜念，並給予大腦刺激來促使未知之力的覺醒。真是極度合理的修行方法。

啊啊、視野變得模糊了。

簡直像腦震蕩了一樣。

以飄忽的、仿彿在空中漫步似的步伐，我準備離開森林。

就在這時，忽然搖曳的光芒出現在了我的眼前。

就好像浮游於空中一樣穿行著的兩道光芒。真是不可思議的光芒。簡直就像在引誘我似得可疑地閃爍著。

「魔、魔力……？」

我用踉蹌的步伐向其走去。

這一定……一定是魔力！

我終於找到未知的力量了！

不知何時步行變成了奔跑，就算被樹根絆倒腳，也任由身體前傾連滾帶爬地，就像野獸那樣奔跑起來。

「魔力！魔力！魔力！ 魔力魔力魔力魔力魔力！！！！」

我衝到兩道光芒前想要抓到它……

「啊……？」

車前燈把世界染成一片白色。尖銳刺耳的剎車音回響在腦海中。

衝擊貫穿了身體、我的……魔力………

◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

就結果而言，我成功找到了魔力。

一醒來就發現周圍充滿了魔力。雖然感覺和最後見到的那兩道光芒有點不同、嘛反正不是什麼大問題。

啊、對對，雖然同樣也不是什麼大不了的問題，但我好像轉生了的樣子。一定是發現魔力後打開了轉生之門什麼的吧。隨便怎樣都好。

總之現在的我是剛出生幾個月的男嬰。意識變得清晰也是最近的事，時間的感覺也還很曖昧，不清楚正確的狀況。最重要的是語言不通，但只要知道文明程度和中世紀歐洲差不多就足夠了吧。

畢竟我終於找到了魔力。這就是一切，過程和附贈品我都沒興趣。

意識覺醒後，我就立刻察覺到了魔力的存在。那漂浮在空中的發光粒子，和前世修行時為了尋找精靈，而全*裸在花圃裡跑來跑去的時候、所感受到的簡直如出一轍。

那個修行並不是徒勞的。作為證據，我立刻就感受到了魔力，現在都已經可以像自己的手腳般那樣自如地操作它了。這種感覺是參考耶穌全*裸釘在十字架上時感受到的……不、是重複改信，以全*裸舞蹈奉上祈禱的時候嗎……恐怕所有的修行都產生了效果吧。能夠強化身體這一點也已經得到了確認。

把嬰兒那多到過剩的時間用於修行，這次我一定要成為影之實力者……啊、要拉屎了。

說起來鳥類好像是不會憋屎直接拉的，人類的嬰兒其實也差不多。不管理性怎樣抗拒，本能卻低語著叫你拉出來。但是朝夕修行的我可以用身體強化來憋緊肛門括約肌爭取時間，而這段時間……

「哦哇啊啊啊啊啊啊啊！！」

可以叫人過來。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

大概過了十年左右吧。

魔力真是厲害。能夠輕易做到超出人類極限的動作。能輕鬆的舉起岩石，跑出比馬還要快數倍的速度，跳的比房子還要高。但是果然還是做不到臉接核彈。雖然可以通過魔力來強化防御力，但果然地球兵器的火力還是很偉大的啊。雖然也不是沒有想過、這個世界又沒有核彈不也無所謂嗎，但做出妥協的『影之實力者』又真的有價值嗎。

不、根本沒有。

為此我的目標是擁有能夠勝過核彈的力量。

而在經過日益研究與修行後，感覺或許能行的方案也確實有那麼一個。所以最近一段時間我一直都在進行實驗。

對對、我出生的家庭好像是貴族的樣子。是被稱為魔劍士的、通過魔力強化身體來戰鬥的騎士代代輩出的家系，而我則是家裡寄以厚望的長子……並沒有那樣的事，只是作為平凡的見習魔劍士被養大的而已。『影之實力者』在展示實力時，可不得好好挑選對手和場合才行。沒錯，一切都只是為了那個終將到來的瞬間……

雖說並沒有認真，但作為見習魔劍士的修行還是挺有用的。不僅能夠學習這個世界運用魔力的戰鬥方法，同時也是個能重新審視自身戰鬥方式的機會。

老實說、我前世所學的戰鬥技巧要比這個世界的技術高出好幾倍。更為精煉更為合理。只要看看現代的格闘比賽就很清楚了。無用的技術和動作被淘汰，各種各樣的武術中只有優秀的部分被保留下來得以融合。那才是戰鬥的究極形態。當然啦、那也只不過是在遵守規則的前提下的究極形態。但其精煉的過程則是在龐雜的技術中找出定式，這種方法是完全可以被廣泛應用的。

而與之相比這個世界的技術首先不會跨出國界。其次也不會跨出流派。也有所謂的絕不外傳的秘技。而且就算想要打開大門進行宣傳，也沒有能夠進行傳播的媒體媒介。也就是說技術根本就沒有融合、淘汰、研磨的機會。用一句話來概括的話就是不夠精煉。

但這個世界的戰鬥方法果然還是與原來的世界有著根本上的區別。沒錯，那就是魔力。由於魔力的緣故，基本的肉體能力有著本質上的不同。

就好比力量。單手就可以把人舉起來。這樣一來關節技的常識根本就不通用了。即使把對手壓在身下也僅靠腹肌就能高高躍起。即使擺出防御姿勢，但被對手單腳踹飛的話根本就沒有意義。嗯、關節技根本就不成立呢。

人有人的戰鬥方式，大猩猩則有大猩猩的戰鬥方法，就是這麼回事。

其他還有踏入對方攻擊範圍的速度啦、步伐的距離啦、戰鬥時與對手間相距的間隔啦。或者說這次才是最重要的。格闘技什麼的說到底不過是距離的遊戲。距離和角度，對於拳擊來說是基本的同時，也是其最為重要的要點。

而為了拿捏這個距離還真是花了不少時間。畢竟這個世界的人的攻擊範圍真的好遠啊。戰鬥時即使兩者相距5米左右也沒問題。不、其實只是蹬地蹬的比較遠而已，速度比較快而已。這種心情我很明白啊。我一開始也以為、這就是異世界的戰鬥方式嗎……而感動不已的呢。但其實根本沒有什麼，只是我的防御技術還不成熟而已。

格闘技中也是有的，越是防守差勁的人就越是會想要無意義的拉開距離。

畢竟對手的攻擊很可怕呢，所以躲到對手打不到的的地方就安心了呢。就是因為這樣才會變成那種一下子縮短距離，然後馬上脫離的乏味戰鬥啊。什麼你說這是Hit & Away？真是遺憾、只是反覆著毫無意義的前後運動的話是稱不上Hit & Away的。

對我來說無論是5米還是100米都同樣沒有價值。畢竟無論是哪邊普通的攻擊都打不中啊。6m、7m、10m也都一樣。因為都夠不著所以就走過去拉近距離吧。

但是一旦踏入一定的距離，即使是1mm的單位也將變的意義重大。那既是我的攻擊能夠打中的距離，同時也是能夠對對手的攻擊做出反應的距離。在這距離內的話，無論是角度還是其他的什麼，僅僅只是側身半步就能決定優勢劣勢。所謂的距離就是用來調整這類極限範圍的東西。絕不是什麼隔5米的話就跑過來攻擊、6米的話就跳過來這樣的東西。

不是、說真的，因為被這名為異世界的先入為主的概念、和魔力這樣未知的東西所迷惑，直到最近我才總算固定下屬於自己的距離。

那麼、帶著這樣的感覺我每天都有參與家裡的訓練。我和姐姐還有父親三人，父親負責指導我和姐姐，而我和姐姐則是進行戰鬥訓練的形式。比我大兩歲的姐姐好像很有天賦，照這樣下去繼承家裡的好像會是姐姐的樣子。由於這個世界只要使用魔力的話，就算是女人也很強。所以由女人繼承家業好像也意外的挺常見的。

為此我才每天都『啊誒誒、姐姐真強啊……』邊這麼說著邊被姐姐暴揍。

畢竟不能夠獲勝啊。要問為什麼的話，想要成為能夠獨當一面的『影之實力者』就必須得扮演好平凡的龍套A才行啊。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

由於我每天都過著那樣的生活，白天還有其他身為貴族要學的東西，以及為了扮作龍套A而進行的交際，所以自由時間很少。

於是修行自然就放在了深夜、大家都睡著了以後。理所應當的睡眠時間當然會有所減少，但我通過獨自開發的、將運用魔力的超回復、和冥想結合而成的睡眠方法，而化身為了超短時睡眠者[Short Sleeper]，為此日子過得還算是挺舒適的。

那麼、今天的修行也加油吧。一如既往的在森林進行基礎訓練後，今天還著特殊的項目。

最近、附近的被廢棄的村落好像有人住進來了的樣子。調查下來的結果似乎是一伙有著一定規模的盜賊團。嗯、作為試劍對象正合適。只是野盜的話倒是斬了不少了。但盜賊團規模的話可是一年一次的大活動，真是興奮難耐啊。我的訓練對象[Sparring Partner]一年到頭都處在不足狀態啊，壞人什麼的真是太歡迎了。啊啊、願治安變的更差。壞人可以處以私刑，在這個世界的鄉下意外的很常見。畢竟能夠制裁壞人的人只有在都市裡才有啊。那麼就由我來制裁吧，就是這麼回事。

然後今天同時也是最近試做的新兵器，值得紀念的、投入實戰的日子。其名為史萊姆緊身衣[Slime Bodysuit]。

就讓我說明一下吧，所謂的史萊姆緊身衣究竟為何物。

在這個世界有著所謂的魔力。而人們則使用這種魔力來強化身體和武器來進行戰鬥。可是在實際運用中卻無論如何都會出現浪費。打個比方、將100的魔力注入普通的鐵劍中，而實際能夠傳導的就只有10左右。將近9成的魔力就這麼被白白浪費了。就連相對來說更容易傳導魔力的秘銀[Mithril]劍，也只是傳導50就被稱為高級品了。所以說浪費率非常的高。

而為此我將目光放在了史萊姆[Slime]的身上，史萊姆是那種一看就能明白的，通過魔力來改變形狀、進行活動的魔法生物。而經過調查史萊姆的魔力傳導率居然高達99%。而且由於是液體還能夠自由的改變形狀。我通過狩獵史萊姆將其核心打碎，把剩下的史萊姆果凍用於研究。打爛的史萊姆核心數量早已不下一千，甚至還導致了周邊的史萊姆不足，從而不得不出門遠征的程度。

而最後終於調和成了方便操作、易於強化的史萊姆果凍。並將其成功覆蓋全身做成了緊身衣。和鎧甲不同即輕便又沒有聲響、非常的舒適，甚至還能輔助身體的動作。當然防御力也是有一等一的。

現在我正穿著用、有黑色素的史萊姆果凍制成的黑色緊身衣[Black Bodysuit]。沒有多餘的裝飾，和身體非常貼身。視野和呼吸也都非常的良好。簡直就和某偵探漫畫中的犯人一模一樣。考慮到今後作為『影之實力者』介入事件的時候，說不定也該琢磨起與之相應的設計了。

總之這樣那樣的，我現在抵達了廢村。明明是深夜卻還亮著燈，看來好像是襲擊商隊的計劃成功了於是在舉辦宴會的樣子。嗯、運氣真的很好。盜賊通常都沒有什麼計劃性，搶奪來的東西馬上就會被揮霍一空。也只有剛襲擊之後才會有像樣的物資。盜賊的東西就是我的東西，將來作為『影之實力者』的資產就是像這樣增加的。

我就這樣以興奮度MAX的狀態突入了宴會。偷襲什麼是不會用的，畢竟那樣根本就算不上練習。

「嘻哈哈—！！給我把值錢的東西都交出來！！」

我在宴會的中心這麼喊道。

「什、什麼啊，這個矮子！」

畢竟只有10歲矮也是正常的。

「噢啦、我說了把錢交出來吧！」

在把我叫做矮子的沒禮貌的男人踹飛後，盜賊們才總算是紛紛拿起了武器。

「喂、敢小看我們的話就算是小鬼我們也不會……！」

「噢啦！」

輕而易舉的砍飛了絮絮叨叨的男人的腦袋。當然了武器也是史萊姆制的，是能只在必要的時候拿出來的優等品。而且這個史萊姆劍[Slime Sword]還有別的便利功能。

便利功能其一、能伸長。

「噢啦噢啦噢啦噢啦噢啦啊啊！」

我伸長史萊姆劍將周圍的龍套盜賊一掃而淨。

像鞭子那樣靈活，但鋒利度卻還是和劍一樣。雖然第一次投入實戰多少有那麼點不安，但這樣看下來似乎沒什麼問題，完全經得住實用。

「噢啦噢啦噢啦噢啦啊啊……啊咧？」

得意忘形砍的太歡樂，不知何時周圍變的靜悄悄得了。啊咧、怎麼好像只剩下一個人了？

「你、你這傢伙到底是什麼人……？」

「沒辦法了。便利功能其二就用你來試吧」

「你、你說的什麼鬼話……！？」

「看起來你至少比這些傢伙要強一點，是類似於Boss的存在嗎？雖然很遺憾、你並沒有能戰勝我的可能性，但當我的練習對手的話至少還能多活2分鐘左右。嘛、就盡量加油吧」

「別、別小看我啊小鬼！我好歹也是在王都……！」

「好的。別說些有的沒的了快放馬過來」

「開什麼玩笑！」

Boss A一臉憤怒的衝了過來。而那慢吞吞的斬擊自然是……沒有避開。

然後Boss A的劍劃過我的胸口，我則因為衝擊倒在了地上。

「哈哈、就是因為小看我才會變成這樣的。我可是在王都拿到武心流免許皆伝的……什、什麼！？」

「才沒被砍到……什麼的」

我像是什麼都沒有發生似的站了起來。

防御機能也相當滿足，看來Boss A程度的攻擊史萊姆緊身衣能夠將其完全無效化的樣子。

「武心流好像最近在王都很流行呢。還真想看看啊」

「該死、就讓你看看好了！」

Boss A的攻擊。

咿呀，嗯、游刃有餘。雖然Boss A拼盡全力地揮著劍，但我甚至沒有擺出架勢的必要。只需要活動身體和腳步就足夠足夠了。

但是叫武心流來著，這劍法我好像還挺喜歡的。

能夠從中窺見這個世界少見的、不被精神論和陳舊的定式所束縛，而是以理來戰鬥姿態。即使是Boss A那樣鈍拙的斬擊也能看得出來。瞬間加速、向前半步、下盡苦功鑽研戰鬥的姿態確實能讓人感受到某種共感。只是、要運用好這種劍術Boss A是完全不夠格的。

看到Boss A中斷了攻擊，我也拉開了與其的距離。

「我、我的劍……為什麼砍不到！」

「畢竟比我家老爸還要弱啊。雖然再怎麼說還是比姐姐要強一點，但照這樣下去再過個1年左右就會被超過了啊」

「臭小鬼！」

彈開破罐破摔砍過來的Boss A的劍，我輕輕地踢了一下他的小腿。使上腳踝的力迅速的、只用膝蓋以下的部位踢出一擊。

於是乎。

「咕、啊、啊啊、為什麼……？」

Boss A抱著小腿呻吟了起來。紅色的血從小腿流出來染紅了地面。

機關非常的簡單，我的腳尖伸出了猶如冰鎬一般鋒利的劍。史萊姆劍的便利功能其二，可以在任何時候、任何地方伸出劍。而在這其中我最為感覺到可能性的使用方法，便是用腳尖的劍踢對方伸到前面的腳的戰法。要防御對方的足部很難。邊防御、封住對方的劍，而另一方面用腳踢。雖然很不起眼但卻很實用。

「沒有再繼續下去的意義了嗎」

「等、等等……！」

「連2分鐘也沒到呢」

這麼說著我用腳尖的劍踢向了Boss A的下巴。串刺之刑。

痙攣著的Boss A向後倒去，我則搜刮起了戰利品。

「美術品我可沒法處理啊—、食品也pass、現金寶石貴金屬come」

戰利品有數輛馬車的份。還有好幾具商人的屍體。

「仇幫你們報了，商品我會替你們有效活用的，就安心去吧」

弄到了還算不錯的戰利品後，我默默的替他們獻上祈禱。換算成現金的話大概500萬澤尼。1澤尼基本和1円的價值差不多。這些全都會成為『影之實力者』的活動資金。要是治安再差點，變成那種盜賊蔓延的世界就好了呢。最好是像遊戲那樣走著走著就能刷出來的那種程度。

「你們來世就再加把勁，爭取蔓延世界吧」

我對再也不能開口的Boss A豎起大拇指……而在視線的前方卻看到了某樣東西。

「牢籠……嗎？」

看起來還挺大挺結實的。

「奴隷嗎？沒法處理也只能pass就是了」

但抱著說不定會有什麼好東西的想法，我姑且還是把蓋在牢籠上的東西揭了下來。

「這還真是……預料之外啊」

在裡面的東西該怎麼說呢……是腐爛的肉塊。姑且還算是保留著人型，但不管是性別還是年齡都已經看不出來了。

可是、還活著。不對、說不定還有意識也不一定。對窺視牢籠的我做出反應肉塊顫抖了下。

確實有聽說過。被稱作是<惡魔憑依>、而被教會處刑了的怪物。原本是作為普通的人類而生，但從某一日起肉體突然開始腐爛。放著不管的話用不了多久就會死，可教會還是會花錢買下活著的<惡魔憑依>，對其進行名為淨化的處刑。悪魔的浄化，雖然其實就是屠宰病人罷了，但民眾卻為之喝彩、擁護教會認為他們守護了和平。當真就是中世紀的感覺，真讓人興起啊。把這個肉塊賣給教會，肯定能得到更甚於今天戰利品的價值吧。當然、我處理不了所以根本沒有意義就是了。

那麼、就讓你解脫吧。

我將史萊姆劍伸入牢籠的縫隙中……突然注意到了某件事。

這個肉塊、其中蘊含著大量的魔力。比幼時起就開始鍛鍊魔力的我還要有過之而無不及，確確實實是如同怪物一般的魔力。然後這個感覺是……。

「這個波長、是魔力暴走了嗎……？」

這個肉塊之所以會變成這樣，難道不是魔力暴走的緣故嗎。過去我也曾經因為魔力暴走的緣故而吃過苦頭。如果那個時候，沒有能抑制住魔力暴走的話，我怕不是也會變成這個樣子。

然後、魔力對肉體造成的影響。我從那天起，便意識到了這種可能性。是否能夠通過有意識的讓魔力暴走，從而使肉體適應魔力，促進變異使其能更容易得操縱魔力呢。可由於魔力的暴走實在是太危險了，所以最終還是放棄了。

但是如果、這個肉塊是魔力暴走後的產物的話，那用這個肉塊來進行實驗的話……我應該能在沒有風險的情況下，更接近『影之實力者』的才是。

「這坨肉、能用啊……」

我向著肉塊伸出手，將魔力注入進去。

◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

差不多過了一個月吧……

回想著得到肉塊那天所發生的事，我在廢棄村發出了與那天同樣的嘆息。

為什麼會變成這樣。

肉塊實驗直到途中為止都非常順利。反正不是自己的身體可以隨心所欲、抱著那樣的想法我不斷地注入了魔力。這也不對那也不對地、進行著那讓人雀躍的實驗的時光。超開心的。不斷接近魔力的神髓，能夠明顯的感到自己的實力在不斷提升這件事，對我來說是無上的喜悅。更加縝密地、更加纖細地，更加強勁地，將魔力的制御提升至極。然後在終於完全制御住魔力暴走的那一瞬間……金髮的精靈[Elf]少女出現在了那裡。

哎呀、太過專注於魔力制御，直到那一刻為止都完全沒察覺到肉塊是金髮的精靈呢。真厲害呢、那種腐爛的肉塊居然還能變回原樣。明明都用『你已經自由了所以快回故鄉吧，願你的未來充滿幸福』這樣的感覺，想把她給送走的說，沒想到居然會說出什麼已經無法回故鄉了，要報答救了她的恩情之類的話來。不、這只是偶然的產物，真的沒打算救你啦。

因為很麻煩所以也考慮過逃走，但最後還是收她做了『影之實力者』的部下A。畢竟看起來也不像會背叛的樣子，而且頭腦貌似也不錯，而且渾身莫名地散發著一種超級能幹的氛圍。明明年齡和我一樣是10歲的樣子，看來精靈的精神比較早熟並不是騙人的。

「以上、從今天起你就是阿爾法[Alpha]了」

A、阿爾法，怎樣都好。

「明白了」

她點了點頭。金髮、藍瞳、白肌膚、美人，非常典型的精靈。

「然後你的工作則是……」

我稍微緘口陷入了思考。這裡非常關鍵。她的工作是輔佐『影之實力者』，這一點上沒有問題。那麼說到底『影之實力者』到底是什麼，『影之實力者』的目的到底是什麼，這就關係到我在這個世界作為目標的『影之實力者』的根基設定了。

設定非常重要。要是戰鬥的理由、是因為打柏青哥輸了泄憤的話，可就沒法裝帥了啊。這一點上我完全沒有疏漏。畢竟無論是來這個世界之前，還是來這個世界之後，我都在不斷地妄想著何為最棒的『影之實力者』。將至今為止考慮過的數千、數萬種有關『影之實力者』的設定進行組合，我瞬間就抵達了最優的解答。

「暗中阻止魔人迪亞波羅斯的復活」

「魔人、迪亞波羅斯……？」

(插圖005)

阿爾法稍傾了下腦袋。

「你應該也知道吧。在遙遠的過去，世界曾因魔人迪亞波羅斯而瀕臨崩壞的危機。但是從人類、精靈、獸人中奮起的三位勇者將迪亞波羅斯擊敗，守護了世界」

「當然知道。但那不是童話嗎？」

「不、全都是真實發生過的事。只不過、事實比起童話可要複雜的多……」

我這麼說著，露出了苦笑。到我這個水準、要建立起揉合了這個世界傳說的『影之實力者』的設定可是非常easy的事。

「被勇者打倒的迪亞波羅斯，在臨死之前對三位勇者下了詛咒。那就是<迪亞波羅斯的詛咒>」

「<迪亞波羅斯的詛咒>？那種東西從沒聽說過啊」

「<迪亞波羅斯的詛咒>是真實存在的。<惡魔憑依>……也就是侵蝕了你身體的疾病」

「誒、怎麼會……」

因為驚愕睜大了雙眼的阿爾法。

「打倒了魔人迪亞波羅斯的英雄子孫們，將永久地受到了這個疾病的折磨。但是、曾經<迪亞波羅斯的詛咒>是能夠治好的東西。就像你這樣」

取回了毫無傷痕的肌膚，讓人無法相信直到最近為止都還是<惡魔憑依>的阿爾法的存在。正是我所言非虛的最好佐證。

當然這些全都是瞎扯的。

「<惡魔憑依>是英雄子孫的證明。過去作為拯救了世界之人的後代被細心保護、感謝、稱頌。曾經的確是這樣呢」

「但是現在別說是被感謝了、甚至……」

阿爾法表情扭曲地說到。

「有什麼人刻意歪曲了歷史。隱藏起那是英雄證明的事實、以及治療詛咒的方法。甚至將其貶低為<惡魔憑依>這種為人蔑視的存在」

「……！究竟是誰！」

「那便是謀圖復活魔人迪亞波羅斯之人。被<迪亞波羅斯的詛咒>所侵蝕之人，無一例外的、都濃厚地繼承了擁有強大魔力的英雄血液。對人類而言是貴重的戰力，可對他們來說卻是礙手礙腳的存在」

「所以才會聲稱<惡魔憑依>將之肅清……」

「沒錯。你背負著<惡魔憑依>這種虛偽的罪狀，失去了故鄉和家族。不感到憎恨嗎？」

「當然恨。怎麼可能不恨啊」

「迪亞波羅斯教團。那便是我們敵人。他們絕不會登上表舞台。因此我們也要潛入陰影之下。潛伏於影、狩獵陰影」

「絕不在表舞台現身，卻還能擁有此等影響力的存在嗎。這麼說來敵人是位高權重之人……而不知真相**作的人應該也不計其數……」

我鄭重地點了點頭。

「這將會是非常艱辛的道路吧。但是、我們必須要實現。願意協助我嗎？」

「若你如此所願的話，那我也賭上這條性命吧。然後給予罪人以、死的制裁……」

阿爾法用那藍色的眼瞳凝視著我、露出了無畏地笑容。那稚嫩卻又美麗的臉龐上、充斥著覺悟和決意。

我在心中擺出了勝利的姿勢。

好咧、這個精靈超好搞定的啊！

迪亞波羅斯教團什麼的當然是不存在的，即使再怎麼找也不可能找得到。所以適當地給哪裡的盜賊背上疑罪然後殺掉，之後則是在看上去像是主人公的傢伙們戰鬥時，隱藏身份亂入進去『這個世界正面臨崩壞的危機……』啥的『魔人的復活即將來臨……』啥的，總之說一通有的沒的、引人遐想的話之後再揚長而去什麼的。又或者颯爽的出現在戰場上，說著『真是愚蠢……**控的傀儡啊……』之類的台詞，然後將其一掃而盡之類的。啊啊、夢想在不斷地延伸啊。

對了、關鍵的組織名…………

「我等是『Shadow Garden』……潛伏於影、狩獵陰影之人……」

「『Shadow Garden』。不錯的名字呢」

對吧？這起名品味簡直超群。

就在今天這個瞬間、『Shadow Garden』成立了。順帶世界的敵人、迪亞波羅斯教團也一同誕生了。我距離『影之實力者』又前進了一步。

「嘛、總而言之先一邊鍛鍊魔力的制御，然後進行劍的練習吧。雖然主要的戰鬥都由我來，但雜魚戰還是要交給你的，所以你也得強到一定程度呢」

「我明白。敵人非常強大，戰力的提升是必須的呢」

「沒錯沒錯、就是那樣」

「也有必要找到其他英雄的子孫將其保護起來呢」

「誒、啊啊，嘛……適當地呢」

雖然『影之實力者』paly由複數人來玩的話，確實比較有組織感設定也更有深度，但是不需要那麼多啦。不如說只有兩人也完全沒問題。

「嘛、總之先專注於變強吧」

我架起木劍，接下了讓人無法想像直到最近為止都還是外行的、那般銳利的阿爾法的劍。才能不錯、魔力也很充分，看起來還挺好使的。

在月光之下，我這樣想著的同時揮下了木劍。
